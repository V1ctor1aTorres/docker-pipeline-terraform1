# Projeto Docker-Pipeline-Terraform-1
Simulação onde o desenvolvedor disponibiliza a imagem de uma aplicação web no Dockerhub e o profissional de devops precisa criar um pipeline para essa aplicação em um container Docker na AWS.

## Criar uma imagem docker:
- Criar a imagem contendo a aplicação e o dockerfile, com as definições de um servidor apache(httpd).
- Usar o comando **docker build** para criar a imagem.
- Usar o comando  **docker run** para testar a imagem localmente.

## Subir a imagem para o DockerHub:
- Caso não esteja logado no docker, usar o comando docker login e fornecer usuário e senha.
- Usar o comando **docker push** para subir a imagem no Dockerhub.
- [Link da imagem no Dockerhub.](https://hub.docker.com/repository/docker/victoriaviana/app-docker/general)

## Subir o servidor na AWS com o Terraform e conectá-lo à um repositorio no Gitlab:
- Criar os arquivos de configuração do Terraform: **main.tf**, **ec2.tf**, **securitygroup.tf**
e **script.sh**. 
- Criar um repositório no Gitlab com os arquivos terraform. - Vincular o repositório ao servidor, através do arquivo script.sh.
- Aplicar os comandos **Terraform init**, **terraform plan** e **terraform  apply**.
- Na console AWS, conectar à EC2 criada e verificar se o docker foi instalado (**sudo docker –version**), se  o gitllab runner foi instalado(**cat /etc/passwd**), e se o gitlab runner tem a permissão necessária para executar o docker(**cat /etc/group**).

## Construir o pipeline:
- Criar o arquivo de pipeline, **.gitlab-cy.yml**, que vai baixar a imagem do Dockerhub e executá-la no servidor na AWS.
- Podemos também criar o arquivo **.gitignore**, usado para que o Git não suba certos trechos do seu projeto para o repositório.

## Testar se a aplicação está disponível no servidor, verificando o endereço IPv4 público da EC2.
![EC2](./images/ec2.png)

![Aplicação Web](./images/site.png)
